package net.tncy.bgi.bookmarket.data;

public class Bookstore {

    private String nameStore;
    private String owner;
    private String address;
    private InventoryEntry inventory;

    public Bookstore(String nameStore, String owner, String address, InventoryEntry inventory) {
        this.nameStore = nameStore;
        this.owner = owner;
        this.address = address;
        this.inventory = inventory;
    }

    public String getNameStore() {
        return nameStore;
    }

    public void setNameStore(String nameStore) {
        this.nameStore = nameStore;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public InventoryEntry getInventory() {
        return inventory;
    }

    public void setInventory(InventoryEntry inventory) {
        this.inventory = inventory;
    }

    @Override
    public String toString() {
        return "Bookstore{" +
                "nameStore='" + nameStore + '\'' +
                ", owner='" + owner + '\'' +
                ", address='" + address + '\'' +
                ", inventory=" + inventory.toString() +
                '}';
    }
}
