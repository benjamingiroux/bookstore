package net.tncy.bgi.bookmarket.data;

import java.util.List;

public class InventoryEntry {

    private int idList;
    private List<Book> listBooks;

    public InventoryEntry(int idList, List<Book> listBooks) {
        this.idList = idList;
        this.listBooks = listBooks;
    }

    public int getIdList() {
        return idList;
    }

    public void setIdList(int idList) {
        this.idList = idList;
    }

    public List<Book> getListBooks() {
        return listBooks;
    }

    public void setListBooks(List<Book> listBooks) {
        this.listBooks = listBooks;
    }

    @Override
    public String toString() {
        return "InventoryEntry{" +
                "idList=" + idList +
                ", listBooks=" + listBooks +
                '}';
    }
}
