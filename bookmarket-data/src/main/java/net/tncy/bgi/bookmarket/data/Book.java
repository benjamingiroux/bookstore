package net.tncy.bgi.bookmarket.data;
import net.tncy.bgi.validator.ISBN;

public class Book {
        private String title;
        private int nbPages;
        private String summary;
        private String author;
        @ISBN
        private String isbn;

        public Book(String title, int nbPages, String summary, String author, String isbn) {
                this.title = title;
                this.nbPages = nbPages;
                this.summary = summary;
                this.author = author;
                this.isbn = isbn;
        }

        public String getTitle() {
                return title;
        }

        public void setTitle(String title) {
                this.title = title;
        }

        public int getNbPages() {
                return nbPages;
        }

        public void setNbPages(int nbPages) {
                this.nbPages = nbPages;
        }

        public String getSummary() {
                return summary;
        }

        public void setSummary(String summary) {
                this.summary = summary;
        }

        public String getAuthor() {
                return author;
        }

        public void setAuthor(String author) {
                this.author = author;
        }

        public String getIsbn() {
                return isbn;
        }

        public void setIsbn(String isbn) {
                this.isbn = isbn;
        }

        @Override
        public String toString() {
                return "Book{" +
                        "title='" + title + '\'' +
                        ", nbPages=" + nbPages +
                        ", summary='" + summary + '\'' +
                        ", author='" + author + '\'' +
                        ", isbn='" + isbn + '\'' +
                        '}';
        }
}
